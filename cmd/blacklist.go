package main

import (
	"bufio"
	"fmt"
	"github.com/bertof/blacklist/pkg/client"
	"github.com/bertof/blacklist/pkg/server"
	"os"
	"strconv"
	"strings"
)

func main() {
	s := server.New()
	s.Run(5)

	c1 := client.New(1)
	c2 := client.New(2)
	c3 := client.New(3)

	s.Register(c1)
	s.Register(c2)
	s.Register(c3)

	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Action:\t")
		text, _ := reader.ReadString('\n')
		text = strings.Trim(text, "\n")

		if text == "message" {
			fmt.Print("Recipient id:\t")
			text, _ := reader.ReadString('\n')
			text = strings.Trim(text, "\n")

			id, _ := strconv.ParseUint(text, 10, 64)

			fmt.Print("Message:\t")
			message, _ := reader.ReadString('\n')
			message = strings.Trim(message, "\n")

			m := client.NewMessage(message, c1, uint(id))
			*s.Receiver() <- &m

		} else if text == "benchmark" {
			fmt.Print("Number of messages:\t")
			text, _ := reader.ReadString('\n')
			text = strings.Trim(text, "\n")
			many, _ := strconv.ParseUint(text, 10, 64)

			fmt.Print("Recipient id:\t")
			text, _ = reader.ReadString('\n')
			text = strings.Trim(text, "\n")
			id, _ := strconv.ParseUint(text, 10, 64)

			fmt.Print("Message:\t")
			message, _ := reader.ReadString('\n')
			message = strings.Trim(message, "\n")

			for i := uint(0); i < uint(many); i++ {
				//go func(index uint) {
				m := client.NewMessage(message, c1, uint(id))
				*s.Receiver() <- &m
				//}(i)
			}

		} else if text == "ban" {
			fmt.Print("Who:\t")
			text, _ := reader.ReadString('\n')
			text = strings.Trim(text, "\n")

			id, _ := strconv.ParseUint(text, 10, 64)
			s.Ban(uint(id))

		} else if text == "unban" {
			fmt.Print("Who:\t")
			text, _ := reader.ReadString('\n')
			text = strings.Trim(text, "\n")

			id, _ := strconv.ParseUint(text, 10, 64)
			s.UnBan(uint(id))

		} else if text == "exit" {
			break
		}
	}
}
