package client

import "fmt"

type Message struct {
	To              Id     `json:"to"`
	Content         string `json:"content"`
	responseChannel *chan *ResponseMessage
}

func NewMessage(content string, from *Client, to Id, ) Message {
	return Message{
		To:              to,
		Content:         content,
		responseChannel: from.responseChannel,
	}
}

func (m *Message) ResponseChannel() *chan *ResponseMessage {
	return m.responseChannel
}

func (m *Message) String() string {
	return fmt.Sprintf("%d <- M[%s]", m.To, m.Content)
}

type ResponseMessage struct {
	Content string `json:"content"`
}

func (r *ResponseMessage) String() string {
	return fmt.Sprintf("R[%s]", r.Content)
}
